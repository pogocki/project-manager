<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('projects', '\App\Models\Project\ProjectApiController')->only([
    'index', 'show',
]);
Route::resource('tasks', '\App\Models\Task\TaskApiController')->only([
    'index', 'show',
]);
Route::prefix('/admin')->group(function () {
    Route::resource('projects', '\App\Models\Project\ProjectAdminApiController');
    Route::resource('tasks', '\App\Models\Task\TaskAdminApiController');
});
