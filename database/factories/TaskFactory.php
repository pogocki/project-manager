<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Task\Task;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->sentence,
        // 'project_id' => factory(\App\Models\Project\Project::class),
        'priority' => $faker->numberBetween(0, 100),
        'done' => $faker->boolean(50),
    ];
});
