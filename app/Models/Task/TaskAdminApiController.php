<?php

namespace App\Models\Task;

use App\Http\Controllers\Controller;
use App\Models\Task\Task;
use Illuminate\Http\Request;

class TaskAdminApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::with('project')->get();
        return response()->json($tasks);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = \Validator::make($input,
            Task::$rules);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 422/*validation failed*/);
        } else {
            $task = new Task($input);
            $task->project_id = $input['project']['id'];
            $task->save();
            return response()->json($task->with('project')->where('id', $task->id)->first());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {

        return response()->json($task->with('project')->where('id', $task->id)->first());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $input = $request->all();
        $validator = \Validator::make($input,
            Task::$rules);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 422/*validation failed*/);
        } else {
            $task->fill($input);
            $task->save();
            return response()->json([]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $task->delete();
        return response()->json([]);
    }
}
