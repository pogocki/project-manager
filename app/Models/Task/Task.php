<?php

namespace App\Models\Task;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public $table = 'tasks';
    
    public $fillable = [
        'name',
        'description',
        'project_id',
        'priority',
        'done',
    ];

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'project_id' => 'integer',
        'priority' => 'integer',
        'done' => 'boolean',
    ];

    public static $rules = [
        'name' => 'required|max:100',        
        'description' => 'max:255',
    ]; 
    
    public function Project()
    {
        return $this->belongsTo(\App\Models\Project\Project::class);
    }
}
