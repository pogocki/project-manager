<?php

namespace App\Models\Task;

use App\Http\Controllers\Controller;
use App\Models\Task\Task;
use Illuminate\Http\Request;

class TaskApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::with('project')->get();
        return response()->json($tasks);
    }  

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {

        return response()->json($task->with('project')->where('id', $task->id)->first());
    }

}
