<?php

namespace App\Models\Project;

use App\Http\Controllers\Controller;
use App\Models\Project\Project;
use Illuminate\Http\Request;

class ProjectApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($path = null)
    {
        $projects = Project::with('tasks')->get();
        return response()->json($projects);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        return response()->json($project->with('tasks')->where('id', $project->id)->first());
    }

}
