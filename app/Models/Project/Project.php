<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public $table = 'projects';

    public $fillable = [
        'name',
        'description',
    ];

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
    ];

    public static $rules = [
        'name' => 'required|max:100',
        'description' => 'max:255',
    ];

    public function Tasks()
    {
        return $this->hasMany(\App\Models\Task\Task::class, 'project_id');
    }
}
