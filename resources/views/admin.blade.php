@extends('layouts.app')

@section('scripts')
<script src="{{ asset('js/admin.js') }}" defer></script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Admin panel') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <div id="admin_panel"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection