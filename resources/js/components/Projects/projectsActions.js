import axios from 'axios';

export const updateProjects = (projects) => {
    return {
        type: 'PROJECTS.UPDATE',
        projects
    }
}

export const loadProjects = () => {
    return (dispatch) => {
        axios.get('/api/projects')
            .then(response => {
                dispatch(updateProjects(response.data));
            });
    }
}

export const closeProject = () => {
    return {
        type: 'PROJECT.CLOSE'
    }
}


const storeProject = (dispatch, project) => {
    return new Promise((resolve, reject) => {
        if (project.id) {
            axios.put(`/api/admin/projects/${project.id}`, project)
                .then(response => {
                    resolve();
                }, error => {
                    reject();
                });
        }
        else {
            axios.post('/api/admin/projects', { ...project }).then(response => {
                dispatch(changeProject(response.data));
                resolve();
            }, error => {
                reject();
            });
        }
    });
}


export const saveProject = (project) => {
    return dispatch => {
        return storeProject(dispatch, project);
    }
}
export const saveAndCloseProject = (project) => {
    return dispatch => {
        return storeProject(dispatch, project)
            .then(() => {
                dispatch(closeProject());
            })
    }
}

export const openProject = (project) => {
    return {
        type: 'PROJECT.OPEN',
        project
    }
}


export const editProject = (projectId) => {
    return dispatch => {
        if (projectId < 0)
            dispatch(openProject({}))
        else
            axios.get(`/api/projects/${projectId}`)
                .then(response => {
                    dispatch(openProject(response.data))
                })
    }
}

export const viewProject = (projectId) => {
    return dispatch => {
        axios.get(`/api/projects/${projectId}`)
            .then(response => {
                dispatch(openProject(response.data))
            })
    }
}

export const changeProject = (project) => {
    return {
        type: 'PROJECT.CHANGE',
        project
    }
}

export const deleteProject = (project) => {
    return (dispatch) => {
        if (confirm(`Are you sure, to delete project #${project.id} (${project.name}) ?`)) {
            axios.delete(`/api/admin/projects/${project.id}`)
                .then(response => {
                    dispatch({
                        type: 'PROJECT.DELETE',
                        project
                    });
                }, error => {
                    alert('Cant delete project');
                });
        }
    }
}
