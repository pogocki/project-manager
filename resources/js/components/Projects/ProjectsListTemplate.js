import React from 'react';
import PropTypes from 'prop-types';

import { Spinner } from './../Spinner';

class ProjectsListTemplate extends React.Component {

    static propTypes = {
        projects: PropTypes.array,
        getProjectButtons: PropTypes.func,
        getFooterButtons: PropTypes.func,
    };

    render = () => {
        const { projects, getProjectButtons, getFooterButtons } = this.props;

        return !projects
            ? <Spinner />
            : <div>
                {projects.length == 0
                    ? <h5>No projects</h5>
                    : (
                        <div className="form-group">
                            <ul className="list-group">
                                {projects.map((project, index) => {
                                    return <li
                                        className="list-group-item"
                                        key={project.id}
                                    >
                                        {project.name}
                                        <div className="float-right">
                                            <span className="badge badge-primary badge-pill">{project.tasks ? project.tasks.length : 0}</span>

                                            {getProjectButtons ? <>{' '} {getProjectButtons(project)}</> : null}
                                        </div>
                                    </li>
                                })}
                            </ul>
                        </div>
                    )
                }
                {getFooterButtons
                    ? (
                        <div className="form-group">
                            {getFooterButtons()}
                        </div>
                    )
                    : null
                }
            </div>
    }
}


export default ProjectsListTemplate;