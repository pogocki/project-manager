import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { loadProjects, deleteProject } from './projectsActions';
import { openProjectRoute, newProjectRoute } from './../../routes';
import ProjectsListTemplate from './ProjectsListTemplate';

class ProjectsListAdmin extends React.Component {

    static propTypes = {
        projects: PropTypes.array
    }

    componentDidMount() {
        this.props.loadProjects();
    }

    render = () => {
        const { projects } = this.props;

        return (
            <ProjectsListTemplate
                projects={projects ?? null}
                getProjectButtons={project => {
                    return (
                        <>
                            <Link to={openProjectRoute(project.id)} className="btn btn-primary btn-sm">Edit</Link>
                            {' '}
                            <button type="button" className="btn btn-danger btn-sm" onClick={event => this.props.deleteProject(project)}>Delete</button>
                        </>
                    )
                }}
                getFooterButtons={() => {
                    return (
                        <Link to={newProjectRoute()} className="btn btn-success">New project</Link>
                    )
                }}
            />
        )
    }
}

const mapState = (state) => {
    return {
        projects: state.projectsReducer.projects,
    }
}

const mapDispatch = (dispatch) => {
    return {
        loadProjects: () => dispatch(loadProjects()),
        editProject: (project) => dispatch(editProject(project)),
        deleteProject: (project) => dispatch(deleteProject(project)),
    }
}

export default connect(mapState, mapDispatch)(ProjectsListAdmin);