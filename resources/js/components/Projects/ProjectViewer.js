import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';

import { closeProject, viewProject } from './projectsActions';
import { Spinner } from './../Spinner';
import { projectsRoute, openTaskRoute } from './../../routes';
import { TasksListTemplate } from './../Tasks/TasksListTemplate';

export class ProjectViewer extends React.Component {

    static propTypes = {
        project: PropTypes.object,
    };

    componentDidMount() {
        const projectId = ((this.props.match && this.props.match.params.projectId) ? this.props.match.params.projectId : -1);
        this.props.viewProject(projectId);
    }

    render = () => {
        const { project } = this.props;

        return !project
            ? <Spinner />
            :
            (
                <div className="card">
                    <h5 className="card-header">Project #{project.id}</h5>
                    <div className="card-body">
                        <div className="form-group">
                            <label htmlFor="name">Name</label>
                            <div>{project.name}</div>
                        </div>
                        <div className="form-group">
                            <label htmlFor="description">Description</label>
                            <div>{project.description}</div>
                        </div>
                        <div className="form-group">
                            <label htmlFor="tasks">Tasks</label>
                            <TasksListTemplate
                                tasks={project.tasks ?? []}
                                getTaskButtons={task => {
                                    return (
                                        <>
                                            <Link to={openTaskRoute(project.id, task.id)} className="btn btn-primary btn-sm">View</Link>                                           
                                        </>
                                    )
                                }}
                            />
                        </div>

                        <div className="form-group">
                            <button type="button" className="btn btn-primary" onClick={event => this.props.closeProject()}>Close</button>
                        </div>
                    </div>
                </div>
            )
    }
}

const mapState = state => {
    return {
        project: state.projectsReducer.openedProject,
    }
}
const mapDispatch = (dispatch, props) => {
    return {
        viewProject: (project) => dispatch(viewProject(project)),
        closeProject: () => {
            dispatch(closeProject());
            props.history.push(projectsRoute());
        },
    }
}
export default withRouter(connect(mapState, mapDispatch)(ProjectViewer));