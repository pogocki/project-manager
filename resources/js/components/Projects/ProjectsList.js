import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { loadProjects, viewProject } from './projectsActions';
import ProjectsListTemplate from './ProjectsListTemplate';
import { openProjectRoute } from './../../routes';

export class ProjectsList extends React.Component {

    componentDidMount() {
        this.props.loadProjects();
    }

    render = () => {
        const { projects,
        } = this.props;

        return (
            <ProjectsListTemplate
                projects={projects ?? null}
                getProjectButtons={project => {
                    return (
                        <>
                            <Link to={openProjectRoute(project.id)} className="btn btn-primary btn-sm">View</Link>
                        </>
                    )
                }}
            />
        )
    }
}

const mapState = (state) => {
    return {
        projects: state.projectsReducer.projects,
    }
}

const mapDispatch = (dispatch) => {
    return {
        loadProjects: () => dispatch(loadProjects()),
        viewProject: (project) => dispatch(viewProject(project)),
    }
}

export default connect(mapState, mapDispatch)(ProjectsList);