import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as yup from 'yup';
import { withRouter, Link } from 'react-router-dom';

import { editProject, changeProject, saveProject, saveAndCloseProject } from './projectsActions';
import { deleteTask } from './../Tasks/tasksActions';
import { Spinner } from './../Spinner';
import { TasksListTemplate } from './../Tasks/TasksListTemplate';
import { projectsRoute, openTaskRoute, newTaskRoute } from './../../routes';


class ProjectEditor extends React.Component {

    static propTypes = {
        project: PropTypes.object,
    };

    constructor(props) {
        super(props)
        this.validationSchema = yup.object().shape({
            name: yup.string().required().max(100),
            description: yup.string().max(255),
        });

        this.state = { errors: null }
    }

    componentDidMount() {
        const projectId = ((this.props.match && this.props.match.params.projectId) ? this.props.match.params.projectId : -1);
        this.props.editProject(projectId);
    }

    onChangeProject = event => {
        let project = this.props.project;
        project[event.target.name] = event.target.value;
        this.props.changeProject(project);
        this.validate();
    }

    projectIsEmpty = () => {
        let project = this.props.project;
        return !project.name && !project.description && (!project.tasks || (count(project.tasks) == 0));
    }

    clearErrors = () => {
        this.setState({ errors: null });
    }

    validate = () => {
        return new Promise((resolve, reject) => {
            this.validationSchema.validate(this.props.project).
                then(() => {
                    this.clearErrors();
                    resolve();
                })
                .catch(err => {
                    this.setState({ errors: err.errors });
                    reject();
                });
        })
    }

    closeProject = () => {
        if (this.projectIsEmpty()) {
            this.props.history.push(projectsRoute());
        }
        else
            this.validate()
                .then(() => {
                    this.props.saveAndCloseProject(this.props.project);
                });
    }

    newTask = () => {
        if (!this.props.project.id) {
            this.validate()
                .then(() => {
                    this.props.saveProject(this.props.project)
                        .then(() => {
                            this.props.history.push(newTaskRoute(this.props.project.id));
                        });
                });
        }
        else
            this.props.history.push(newTaskRoute(this.props.project.id));
    }

    render = () => {
        const { project } = this.props;

        return !project
            ? <Spinner />
            :
            (
                <div className="card">
                    <h5 className="card-header">{project.id ? `Project #${project.id}` : 'New project'}</h5>
                    <div className="card-body">
                        <div className="form-group">
                            <label htmlFor="name">Name</label>
                            <input type="text" name="name" className="form-control" value={project.name ?? ''} onChange={this.onChangeProject} required />
                        </div>
                        <div className="form-group">
                            <label htmlFor="description">Description</label>
                            <textarea name="description" className="form-control" value={project.description ?? ''} onChange={this.onChangeProject} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="tasks">Tasks</label>
                            <TasksListTemplate
                                tasks={project.tasks ?? []}
                                getTaskButtons={task => {
                                    return (
                                        <>
                                            <Link to={openTaskRoute(project.id, task.id)} className="btn btn-primary btn-sm">Edit</Link>
                                            {' '}
                                            <button type="button" className="btn btn-danger btn-sm" onClick={event => this.props.deleteTask(task)}>Delete</button>
                                        </>
                                    )
                                }}
                            />
                        </div>
                        {!this.state.errors ? null : this.state.errors.map((error, index) => {
                            return (
                                <div className="alert alert-warning" role="alert" key={index}>
                                    {error}
                                </div>
                            )
                        })}

                        <button type="button" className="btn btn-success" onClick={event => this.newTask()}>New task</button>
                        <div className="float-right">
                            <button type="button" className="btn btn-primary" onClick={event => this.closeProject()}>Close</button>
                        </div>
                    </div>
                </div>
            )
    }
}

const mapState = state => {
    return {
        project: state.projectsReducer.openedProject,
    }
}
const mapDispatch = (dispatch, props) => {
    return {
        editProject: (project) => dispatch(editProject(project)),
        changeProject: (project) => dispatch(changeProject(project)),
        saveProject: (project) => dispatch(saveProject(project)),
        saveAndCloseProject: (project) => {
            return dispatch(saveAndCloseProject(project))
                .then(() => {
                    props.history.push(projectsRoute());
                });
        },
        deleteTask: (task) => dispatch(deleteTask(task)),
    }
}
export default withRouter(connect(mapState, mapDispatch)(ProjectEditor));