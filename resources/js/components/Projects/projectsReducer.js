import React from 'react';

const initialState = {
    projects: null,
    openedProject: null,
}

function projectsReducer(state = initialState, action) {
    switch (action.type) {
        case 'PROJECTS.UPDATE':
            return {
                ...state,
                projects: action.projects,
            }
        case 'PROJECT.OPEN':
            return {
                ...state,
                openedProject: action.project,
            }
        case 'PROJECT.CLOSE':
            return {
                ...state,
                openedProject: null,
            }
        case 'PROJECT.CHANGE':
            return {
                ...state,
                openedProject: { ...action.project },
            }
        case 'PROJECT.DELETE':
            {
                const projects = state.projects.filter(project => {
                    return project.id != action.project.id;
                })
                return {
                    ...state,
                    projects
                }
            }

        case 'PROJECT.TASK.DELETE': {
            let openedProject = state.openedProject;
            openedProject.tasks = openedProject.tasks.filter(task => task.id != action.task.id);
            return {
                ...state,
                openedProject: { ...openedProject },
            }
        }

        default:
            return state
    }
}

export default projectsReducer;