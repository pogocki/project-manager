import React from 'react';
import PropTypes from 'prop-types';
import { Spinner } from './../Spinner';

export const TasksListTemplate = (props) => {
    return !props.tasks
        ? <Spinner />
        : <div>
            {props.tasks.length == 0
                ? <h5>No tasks</h5>
                : (
                    <div className="form-group">
                        <div className="list-group">
                            {props.tasks.map((task, index) => {
                                return <div key={task.id ?? -index} className="list-group-item list-group-item-action">
                                    <div className="d-flex w-100 justify-content-between">
                                        <h5 className="mb-1">{task.id ? `#${task.id} | ` : ''}{task.name}</h5>
                                        <div className="float-right">
                                            {!props.getTaskButtons ? null : props.getTaskButtons(task)}
                                        </div>
                                    </div>
                                    <p className="mb-1">{task.description}</p>
                                    {!task.done ? null : <span className="badge badge-success">Done</span>}
                                </div>
                            })}
                        </div>
                    </div>
                )
            }
            {props.getFooterButtons
                ? (
                    <div className="form-group">
                        {props.getFooterButtons()}
                    </div>
                )
                : null
            }
        </div>
}

TasksListTemplate.propTypes = {
    tasks: PropTypes.array,
    getTaskButtons: PropTypes.func,
    getFooterButtons: PropTypes.func,
}