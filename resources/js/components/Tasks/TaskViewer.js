import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { viewTask } from './tasksActions';
import { Spinner } from './../Spinner';
import { openProjectRoute } from '../../routes';

export class TaskViewer extends React.Component {

    static propTypes = {
        task: PropTypes.object,
    };

    componentDidMount() {
        const taskId = ((this.props.match && this.props.match.params.taskId) ? this.props.match.params.taskId : -1);
        this.props.viewTask(taskId, this.props.match.params.projectId);
    }

    closeTask = () => {
        this.props.history.push(openProjectRoute(this.props.task.project.id));
    }


    render = () => {
        const { task } = this.props;
        return !task
            ? <Spinner />
            : (
                <div className="card">
                    <h5 className="card-header">{`${task.id ? `Task #${task.id}` : 'New task'} | Project #${task.project.id}`}</h5>
                    <div className="card-body">
                        <div className="form-group">
                            <label htmlFor="name">Name</label>
                            <div>{task.name}</div>
                        </div>
                        <div className="form-group">
                            <label htmlFor="description">Description</label>
                            <div>{task.description}</div>
                        </div>
                        <div className="form-group">
                            <label htmlFor="priority">Priority</label>
                            <div>{task.priority}</div>
                        </div>

                        <div className="form-group">
                            {task.done
                                ? <span className="badge badge-success">Done</span>
                                : <span className="badge badge-danger">Not done</span>
                            }
                        </div>

                        <div className="float-right">
                            <button type="button" className="btn btn-primary" onClick={event => this.closeTask()}>Close</button>
                        </div>
                    </div>
                </div>
            )
    }
}

const mapState = (state) => {
    return {
        task: state.tasksReducer.openedTask,
    };
}

const mapDispatch = (dispatch, props) => {
    return {
        viewTask: (taskId, projectId) => dispatch(viewTask(taskId, projectId)),
    };
}


export default withRouter(connect(mapState, mapDispatch)(TaskViewer));