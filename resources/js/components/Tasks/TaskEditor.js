import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import * as yup from 'yup';

import { editTask, changeTask, saveAndCloseTask } from './tasksActions';
import { Spinner } from './../Spinner';
import { openProjectRoute } from '../../routes';

class TaskEditor extends React.Component {

    static propTypes = {
        task: PropTypes.object,
    };

    constructor(props) {
        super(props)
        this.validationSchema = yup.object().shape({
            name: yup.string().required().max(100),
            description: yup.string().max(255),
        });

        this.state = { errors: null }
    }

    componentDidMount() {
        const taskId = ((this.props.match && this.props.match.params.taskId) ? this.props.match.params.taskId : -1);
        this.props.editTask(taskId, this.props.match.params.projectId);
    }

    onChangeTask = event => {
        let task = this.props.task;
        task[event.target.name] = (event.target.type == 'checkbox') ? !task[event.target.name] : event.target.value;
        this.props.changeTask(task);
        this.validate();
    }

    clearErrors = () => {
        this.setState({ errors: null });
    }

    validate = () => {
        return new Promise((resolve, reject) => {
            this.validationSchema.validate(this.props.task).
                then(() => {
                    this.clearErrors();
                    resolve();
                })
                .catch(err => {
                    this.setState({ errors: err.errors });
                    reject();
                });
        })
    }

    taskIsEmpty = () => {
        let task = this.props.task;
        return !task.name && !task.description;
    }


    closeTask = () => {
        if (this.taskIsEmpty()) {
            this.props.history.push(openProjectRoute(this.props.task.project.id));
        }
        else
            this.validate()
                .then(() => {
                    this.props.saveAndCloseTask(this.props.task);
                });
    }

    render = () => {
        const { task } = this.props;
        return !task
            ? <Spinner />
            : (
                <div className="card">
                    <h5 className="card-header">{`${task.id ? `Task ${task.id}` : 'New task'} | Project #${task.project.id}`}</h5>
                    <div className="card-body">
                        <div className="form-group">
                            <label htmlFor="name">Name</label>
                            <input type="text" name="name" className="form-control" value={task.name ?? ''} onChange={this.onChangeTask} required />
                        </div>
                        <div className="form-group">
                            <label htmlFor="description">Description</label>
                            <textarea name="description" className="form-control" value={task.description ?? ''} onChange={this.onChangeTask} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="priority">Priority</label>
                            <input type="number" name="priority" className="form-control" value={task.priority ?? ''} onChange={this.onChangeTask} />
                        </div>

                        <div className="form-check">
                            <input className="form-check-input" name="done" type="checkbox" checked={task.done ?? false} onChange={this.onChangeTask} />
                            <label className="form-check-label" htmlFor="done">
                                Done
                            </label>
                        </div>
                        {!this.state.errors ? null : this.state.errors.map((error, index) => {
                            return (
                                <div className="alert alert-warning" role="alert" key={index}>
                                    {error}
                                </div>
                            )
                        })}

                        <div className="float-right">
                            <button type="button" className="btn btn-primary" onClick={event => this.closeTask()}>Close</button>
                        </div>
                    </div>
                </div>
            )
    }
}

const mapState = (state) => {
    return {
        task: state.tasksReducer.openedTask,
    };
}

const mapDispatch = (dispatch, props) => {
    return {
        editTask: (taskId, projectId) => dispatch(editTask(taskId, projectId)),
        changeTask: (task) => dispatch(changeTask(task)),
        saveAndCloseTask: (task) => {
            return dispatch(saveAndCloseTask(task))
                .then(() => {
                    props.history.push(openProjectRoute(task.project.id));
                });
        },
    };
}


export default withRouter(connect(mapState, mapDispatch)(TaskEditor));