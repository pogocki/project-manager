import React from 'react';

const initialState = {
    openedTask: null,
}

function tasksReducer(state = initialState, action) {
    switch (action.type) {        
        case 'TASK.OPEN':
            return {
                ...state,
                openedTask: action.task,
            }
        case 'TASK.CLOSE':
            return {
                ...state,
                openedTask: null,
            }
        case 'TASK.CHANGE':
            return {
                ...state,
                openedTask: { ...action.task },
            }

        default:
            return state
    }
}

export default tasksReducer;