import axios from 'axios';

export const openTask = (task) => {
    return {
        type: 'TASK.OPEN',
        task
    }
}


export const editTask = (taskId, projectId) => {
    return dispatch => {
        if (taskId < 0) {
            axios.get(`/api/projects/${projectId}`)
                .then(reponse => {
                    dispatch(openTask({
                        project: reponse.data,
                        priority: 0,
                    }))
                });
        }
        else
            axios.get(`/api/tasks/${taskId}`)
                .then(response => {
                    dispatch(openTask(response.data))
                })
    }
}

export const viewTask = (taskId, projectId) => {
    return dispatch => {

        axios.get(`/api/tasks/${taskId}`)
            .then(response => {
                dispatch(openTask(response.data))
            })

    }
}

export const changeTask = (task) => {
    return {
        type: 'TASK.CHANGE',
        task
    }
}

export const saveAndCloseTask = (task) => {
    return dispatch => {
        return new Promise((resolve, reject) => {
            if (task.id) {
                axios.put(`/api/admin/tasks/${task.id}`, task)
                    .then(response => {
                        dispatch(closeTask());
                        resolve();
                    }, error => {
                        reject();
                    });
            }
            else {
                axios.post('/api/admin/tasks', { ...task }).then(response => {
                    dispatch(changeTask(response.data));
                    dispatch(closeTask());
                    resolve();
                }, error => {
                    reject();
                });
            }
        });
    }
}

export const closeTask = () => {
    return {
        type: 'TASK.CLOSE'
    }
}


export const deleteTask = (task) => {
    return (dispatch) => {
        if (confirm(`Are you sure, to delete task #${task.id} (${task.name}) ?`)) {
            axios.delete(`/api/admin/tasks/${task.id}`, task)
                .then(response => {
                    dispatch({
                        type: 'PROJECT.TASK.DELETE',
                        task
                    });
                }, error => {
                    alert('Cant delete task');
                });
        }
    }
}
