import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {
    BrowserRouter as Router,
    Switch, Route
} from "react-router-dom";

import { store } from './store';
import ProjectsListAdmin from './components/Projects/ProjectsListAdmin';
import ProjectEditor from './components/Projects/ProjectEditor';
import TaskEditor from './components/Tasks/TaskEditor';
import * as routes from './routes';

function Admin() {
    return (
        <div>
            <Router basename={window.location.pathname}>
                <Switch>
                    <Route path={routes.newProjectRoute()} exact component={ProjectEditor} />
                    <Route path={routes.openProjectRoute()} exact component={ProjectEditor} />
                    <Route path={routes.newTaskRoute()} exact component={TaskEditor} />
                    <Route path={routes.openTaskRoute()} exact component={TaskEditor} />
                    <Route path={routes.projectsRoute()} component={ProjectsListAdmin} />
                </Switch>
            </Router>
        </div>
    );
}

ReactDOM.render(
    <Provider store={store}>
        <Admin />
    </Provider>, document.getElementById('admin_panel'));