import React from 'react';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import projectsReducer from './components/Projects/projectsReducer';
import tasksReducer from './components/Tasks/tasksReducer';

export const store = createStore(combineReducers({ projectsReducer, tasksReducer }), applyMiddleware(thunk));