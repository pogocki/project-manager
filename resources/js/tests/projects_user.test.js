import React from 'react';
import { shallow, mount } from 'enzyme';
import { BrowserRouter as Router } from 'react-router-dom';

import { ProjectsList } from './../components/Projects/ProjectsList';
import { ProjectViewer } from './../components/Projects/ProjectViewer';

describe("Projects: user components tests", () => {

  it('ProjectsList renders without crashing', () => {
    const props = {
      projects: [],
      loadProjects: () => { },
    }

    shallow(<ProjectsList {...props} />);
  });

  it('ProjectsList renders "No projects"', () => {
    const props = {
      projects: [],
      loadProjects: () => { },
    }

    const component = mount(<ProjectsList  {...props} />);
    expect(component.find('h5').text()).toBe('No projects');
  });

  it('ProjectsList renders projects', () => {

    const props = {
      projects: [
        {
          id: 0,
          name: 'project 1',
          description: 'project 1 description',
        },
        {
          id: 1,
          name: 'project 2',
          description: 'project 2 description',
        },
      ],
      loadProjects: () => { },
    }

    const component = mount(
      <Router>
        <ProjectsList {...props} />
      </Router>);
    expect(component.find('.list-group-item')).toHaveLength(2);
  });

  it('ProjectsList renders projects with proper tasks count', () => {

    const props = {
      projects: [
        {
          id: 0,
          name: 'project 1',
          description: 'project 1 description',
          tasks: [
            {
              id: 0,
              name: 'task 1',
              description: 'task 1 description',
            },
            {
              id: 1,
              name: 'task 2',
              description: 'task 2 description',
            },
            {
              id: 2,
              name: 'task 3',
              description: 'task 3 description',
            },
          ]
        },
      ],
      loadProjects: () => { },
    }

    const component = mount(
      <Router>
        <ProjectsList {...props} />
      </Router>);
    expect(component.find('.badge-pill').text()).toBe('3');
  });

  it('ProjectViewer renders project with "No tasks"', () => {
    const props = {
      project:
        {
          id: 0,
          name: 'project 1',
          description: 'project 1 description',
          tasks: [           
          ]
        },
      
        viewProject: () => { },
    }

    const component = mount(
      <Router>
        <ProjectViewer {...props} />
      </Router>);
    expect(component.find('.form-group h5').text()).toBe('No tasks');

  });

  it('ProjectViewer renders project proper tasks list', () => {
    const props = {
      project:
        {
          id: 0,
          name: 'project 1',
          description: 'project 1 description',
          tasks: [
            {
              id: 0,
              name: 'task 1',
              description: 'task 1 description',
            },
            {
              id: 1,
              name: 'task 2',
              description: 'task 2 description',
            },
            {
              id: 2,
              name: 'task 3',
              description: 'task 3 description',
            },
          ]
        },
      
        viewProject: () => { },
    }

    const component = mount(
      <Router>
        <ProjectViewer {...props} />
      </Router>);
    expect(component.find('.list-group-item')).toHaveLength(3);

  });




});
