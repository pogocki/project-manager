import React from 'react';
import { shallow, mount } from 'enzyme';
import { TaskViewer } from './../components/Tasks/TaskViewer';
import { BrowserRouter as Router } from 'react-router-dom';

describe("Tasks: user components tests", () => {

  it('TaskViewer renders without crashing', () => {
    const props = {
      task: {
        id: 0,
        name: 'task 1',
        description: 'task 1 description',
        project: {
          id: 0,
          name: 'project 1',
          description: 'project 1 description',
        }
      },
      viewTask: () => { },
      match: {params: {projectId: 0}},
    }

    shallow(<TaskViewer {...props} />);
  });

  it('TaskViewer renders task', () => {

    const props = {
      task: {
        id: 1,
        name: 'task 1',
        description: 'task 1 description',
        done: false,
        project: {
          id: 2,
          name: 'project 2',
          description: 'project 1 description',
        }
      },
      viewTask: () => { },
      match: {params: {projectId: 0}},
    }

    const component = mount(
      <Router>
        <TaskViewer {...props} />
      </Router>);
    expect(component.find('h5').text()).toBe('Task #1 | Project #2');
    expect(component.find('.badge').text()).toBe('Not done');
  });


});
