import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {
    BrowserRouter as Router,
    Switch, Route
} from "react-router-dom";

import { store } from './store';
import ProjectsList from './components/Projects/ProjectsList';
import ProjectViewer from './components/Projects/ProjectViewer';
import TaskViewer from './components/Tasks/TaskViewer';
import * as routes from './routes';


function Home() {
    return (
        <div>
            <Router basename={window.location.pathname}>
                <Switch>
                    <Route path={routes.openProjectRoute()} exact component={ProjectViewer} />
                    <Route path={routes.openTaskRoute()} exact component={TaskViewer} />
                    <Route path={routes.projectsRoute()} component={ProjectsList} />
                </Switch>
            </Router>
        </div>
    );
}

ReactDOM.render(
    <Provider store={store}>
        <Home />
    </Provider>, document.getElementById('viewer_panel'));