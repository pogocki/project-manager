export const projectsRoute = () => '/';
export const newProjectRoute = () => '/project/new';
export const openProjectRoute = (projectId = null) => `/project/${projectId ?? ':projectId'}`;
export const openTaskRoute = (projectId = null, taskId = null) => `/project/${projectId ?? ':projectId'}/task/${taskId ?? ':taskId'}`;
export const newTaskRoute = (projectId = null) => `/project/${projectId ?? ':projectId'}/task/new`;
