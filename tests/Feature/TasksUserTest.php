<?php

namespace Tests\Feature;

use Tests\TestCase;

class TasksUserTest extends TestCase
{
    public function test_TaskReturned()
    {
        $project = factory(\App\Models\Project\Project::class)->create();
        $task = factory(\App\Models\Task\Task::class)->make([
            'project_id' => $project->id,
        ]);
        $project->tasks()->save($task);

        $this->json('GET', 'api/tasks/' . $task->id, [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson($task->toArray());
    }

}
