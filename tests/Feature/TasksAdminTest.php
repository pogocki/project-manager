<?php

namespace Tests\Feature;

use Laravel\Passport\Passport;
use Tests\TestCase;

class TasksAdminTest extends TestCase
{
    public function test_TaskReturned()
    {
        $user = factory(\App\User::class)->make();
        Passport::actingAs($user);

        $project = factory(\App\Models\Project\Project::class)->create();
        $task = factory(\App\Models\Task\Task::class)->make([
            'project_id' => $project->id,
        ]);
        $project->tasks()->save($task);

        $this->json('GET', 'api/admin/tasks/' . $task->id, [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson($task->toArray());
    }

    public function test_TaskCreated()
    {
        $user = factory(\App\User::class)->make();
        Passport::actingAs($user);

        $project = factory(\App\Models\Project\Project::class)->create();
        $taskArray = [
            'name' => 'API tests',
            'description' => 'Make all API tests',
            'project_id' => $project->id,
            'priority' => 100,
            'done' => false,
            'project' => $project->toArray(),
        ];

        $this->json('POST', 'api/admin/tasks', $taskArray, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson($taskArray);
    }

    public function test_SingleTaskCreatedAndReturnedAfterReload()
    {
        $user = factory(\App\User::class)->make();
        Passport::actingAs($user);

        $project = factory(\App\Models\Project\Project::class)->create();
        $taskArray = [
            'name' => 'API tests',
            'description' => 'Make all API tests',
            'project_id' => $project->id,
            'priority' => 100,
            'done' => false,
            'project' => $project->toArray(),
        ];

        $this->json('POST', 'api/admin/tasks', $taskArray, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson($taskArray);

        $this->json('GET', 'api/admin/tasks/1', [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson($taskArray);
    }

    public function test_TaskUpdated()
    {
        $user = factory(\App\User::class)->make();
        Passport::actingAs($user);

        $project = factory(\App\Models\Project\Project::class)->create();
        $task = factory(\App\Models\Task\Task::class)->create([
            'project_id' => $project->id,
        ]);

        $newTaskData = [
            'name' => 'API tests',
            'description' => 'Make all API tests',
            'project_id' => $project->id,
            'priority' => 100,
            'done' => false,
            'project' => $project->toArray(),
        ];

        $this->json('PUT', 'api/admin/tasks/' . $task->id, $newTaskData, ['Accept' => 'application/json'])
            ->assertStatus(200);

        $this->json('GET', 'api/admin/tasks/' . $task->id, [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson($newTaskData);
    }

    public function test_TaskFailedInValidation()
    {
        $user = factory(\App\User::class)->make();
        Passport::actingAs($user);

        $project = factory(\App\Models\Project\Project::class)->create();
        $task = factory(\App\Models\Task\Task::class)->create([
            'project_id' => $project->id,
        ]);

        $newTaskData = [
            'name' => '',
            'description' => 'Make all API tests',
            'project_id' => $project->id,
            'priority' => 100,
            'done' => false,
            'project' => $project->toArray(),
        ];

        $this->json('PUT', 'api/admin/tasks/' . $task->id, $newTaskData, ['Accept' => 'application/json'])
            ->assertStatus(422/*validation failed*/);
    }

    public function test_TaskDeleted()
    {
        $user = factory(\App\User::class)->make();
        Passport::actingAs($user);

        $project = factory(\App\Models\Project\Project::class)->create();
        $tasks = factory(\App\Models\Task\Task::class, 10)->create([
            'project_id' => $project->id,
        ]);

        $this->json('DELETE', 'api/admin/tasks/' . $tasks->first()->id, [], ['Accept' => 'application/json'])
            ->assertStatus(200);

        $this->json('GET', 'api/admin/tasks', [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonCount(9);
    }

    public function test_TaskNotCreatedWhenNotAuthorized()
    {
        $project = factory(\App\Models\Project\Project::class)->create();
        $taskArray = [
            'name' => 'API tests',
            'description' => 'Make all API tests',
            'project_id' => $project->id,
            'priority' => 100,
            'done' => false,
            'project' => $project->toArray(),
        ];

        $this->json('POST', 'api/admin/tasks', $taskArray, ['Accept' => 'application/json'])
            ->assertStatus(401/*Unauthorized*/);
    }

    public function test_TaskNotDeletedWhenNotAuthorized()
    {
        $project = factory(\App\Models\Project\Project::class)->create();
        $tasks = factory(\App\Models\Task\Task::class, 10)->create([
            'project_id' => $project->id,
        ]);

        $this->json('DELETE', 'api/admin/tasks/' . $tasks->first()->id, [], ['Accept' => 'application/json'])
            ->assertStatus(401/*Unauthorized*/);
    }

}
