<?php

namespace Tests\Feature;

use Tests\TestCase;
use Laravel\Passport\Passport;

class ProjectsAdminTest extends TestCase
{
    public function test_NotAuthenticatedRedirectsToLogin()
    {
        $response = $this->get('/admin')
            ->assertRedirect('/login');
    }

    public function test_AuthenticatedNotRedirectsToLogin()
    {
        $user = factory(\App\User::class)->make();
        Passport::actingAs($user);
        $response = $this->get('/admin')
            ->assertStatus(200);
    }

    public function test_ProjectsReturned()
    {
        $user = factory(\App\User::class)->make();
        Passport::actingAs($user);

        $projects = factory(\App\Models\Project\Project::class, 10)->create();
        $this->json('GET', 'api/admin/projects', [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonCount(10);
    }

    public function test_ProjectCreated()
    {
        $user = factory(\App\User::class)->make();
        Passport::actingAs($user);

        $projectArray = [
            'name' => 'FrameCoders project',
            'description' => 'FrameCoders test project',
        ];

        $this->json('POST', 'api/admin/projects', $projectArray, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'name' => 'FrameCoders project',
                'description' => 'FrameCoders test project',
            ]);
    }

    public function test_SingleProjectReturned()
    {
        $user = factory(\App\User::class)->make();
        Passport::actingAs($user);

        $project = factory(\App\Models\Project\Project::class)->create();

        $this->json('GET', 'api/admin/projects/1', [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson($project->toArray());
    }

    public function test_SingleProjectCreatedAndReturnedAfterReload()
    {
        $user = factory(\App\User::class)->make();
        Passport::actingAs($user);

        $projectArray = [
            'name' => 'FrameCoders project',
            'description' => 'FrameCoders test project',
        ];
        $this->json('POST', 'api/admin/projects', $projectArray, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'name' => 'FrameCoders project',
                'description' => 'FrameCoders test project',
            ]);
        $this->json('GET', 'api/admin/projects/1', [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'name' => 'FrameCoders project',
                'description' => 'FrameCoders test project',
            ]);
    }

    public function test_ProjectUpdated()
    {
        $user = factory(\App\User::class)->make();
        Passport::actingAs($user);

        $project = factory(\App\Models\Project\Project::class)->create();

        $this->json('GET', 'api/admin/projects/1', [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson($project->toArray());

        $projectData = [
            'name' => 'FrameCoders project',
            'description' => 'FrameCoders test project',
        ];
        $project->fill($projectData);

        $this->json('PUT', 'api/admin/projects/' . $project->id, $project->toArray(), ['Accept' => 'application/json'])
            ->assertStatus(200);

        $this->json('GET', 'api/admin/projects/' . $project->id, [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson($projectData);
    }

    public function test_ProjectFailedInValidation()
    {
        $user = factory(\App\User::class)->make();
        Passport::actingAs($user);

        $projectArray = [
            'name' => '',
            'description' => 'FrameCoders test project',
        ];
        $this->json('POST', 'api/admin/projects', $projectArray, ['Accept' => 'application/json'])
            ->assertStatus(422/*validation failed*/);
    }

    public function test_ProjectDeleted()
    {
        $user = factory(\App\User::class)->make();
        Passport::actingAs($user);

        $projects = factory(\App\Models\Project\Project::class, 10)->create();
        $this->json('DELETE', 'api/admin/projects/' . $projects->first()->id, [], ['Accept' => 'application/json'])
            ->assertStatus(200);

        $this->json('GET', 'api/admin/projects', [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonCount(9);
    }

    public function test_ProjectReturnedWithTasks()
    {
        $user = factory(\App\User::class)->make();
        Passport::actingAs($user);

        $projects = factory(\App\Models\Project\Project::class, 10)
            ->create()
            ->each(function ($project) {
                $project->tasks()->saveMany(factory(\App\Models\Task\Task::class, 3)->make([
                    'project_id' => $project->id,
                ]));
            });
        $this->json('GET', 'api/admin/projects/1', [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonCount(3, 'tasks');
    }

    public function test_ProjectNotCreatedWhenNotAuthorized()
    {
        $projectArray = [
            'name' => 'FrameCoders project',
            'description' => 'FrameCoders test project',
        ];

        $this->json('POST', 'api/admin/projects', $projectArray, ['Accept' => 'application/json'])
            ->assertStatus(401/*Unauthorized*/);
    }

    public function test_ProjectNotDeletedWhenNotAuthorized()
    {
        $projects = factory(\App\Models\Project\Project::class, 10)->create();
        $this->json('DELETE', 'api/admin/projects/' . $projects->first()->id, [], ['Accept' => 'application/json'])
            ->assertStatus(401/*Unauthorized*/);
    }

}
