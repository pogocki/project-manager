<?php

namespace Tests\Feature;

use Tests\TestCase;

class ProjectsUserTest extends TestCase
{

    public function test_NotAuthenticatedDontRedirectsToLogin()
    {
        $response = $this->get('/home')
            ->assertStatus(200);
    }

    public function test_ProjectsReturned()
    {
        $projects = factory(\App\Models\Project\Project::class, 10)->create();
        $this->json('GET', 'api/projects', [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonCount(10);
    }

    public function test_SingleProjectReturned()
    {
        $project = factory(\App\Models\Project\Project::class)->create();

        $this->json('GET', 'api/projects/' . $project->id, [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson($project->toArray());
    }

}
